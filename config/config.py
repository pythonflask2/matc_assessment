from env_settings import get_env


class Config:
    FLASK_ENV = get_env('FLASK_ENV')
    DB_NAME = get_env('DB_NAME')
    DB_HOST = get_env('DB_HOST')
    DB_USER = get_env('DB_USER')
    DB_PASSWORD = get_env('DB_PASSWORD')
    SECRET_KEY = get_env('SECRET_KEY')
    DATABASE_URI = F'mysql+pymysql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}/{DB_NAME}'
    CONNECTION_URL = get_env('CONNECTION_URL')
    # SQLALCHEMY_TRACK_MODIFICATIONS = False


def check_functionality():
    print(Config.FLASK_ENV)
    print(Config.DATABASE_URI)


check_functionality()
