import pymongo
from flasgger import Swagger
from flask_sqlalchemy import SQLAlchemy
from flask import Flask
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker

from config.config import Config
import pymysql

pymysql.install_as_MySQLdb()

db = SQLAlchemy()

app = Flask(__name__)


def create_app():
    app.config['SQLALCHEMY_DATABASE_URI'] = Config.DATABASE_URI
    app.config['CONNECTION_URL'] = Config.CONNECTION_URL
    app.config['SECRET_KEY'] = 'Th1s1ss3cr3t'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = \
        Config.SQLALCHEMY_TRACK_MODIFICATIONS
    from app.models.employee import Employee
    from app.models.person import Person
    from app.models.address import Address
    from app.api.login import login
    from app.api.employee import employee
    from app.api.address import address
    from app.api.person import person
    from app.api.mongo_api import mongo
    from app.api.mongo_person_api import mongo_person
    from app.api.mongo_address_api import mongo_address
    from app.models.paginate import paginate
    app.register_blueprint(mongo, use_prefix='/mongo')
    app.register_blueprint(mongo_address, use_prefix='/mongo_address')
    app.register_blueprint(mongo_person, use_prefix='/mongo_person')
    app.register_blueprint(login, use_prefix='/app/api/login')
    app.register_blueprint(employee, use_prefix='/app/api/employee')
    app.register_blueprint(address, use_prefix='app/api/address')
    app.register_blueprint(person, use_prefix='app/api/person')
    app.register_blueprint(paginate, use_prefix='/app/api/paginate')
    swagger_template = {
        'components': {
            'securitySchemes': {
                'jwt': {
                    'type': 'http',
                    'scheme': "bearer",
                    'in': "header",
                    'name': 'Auth_token'
                },
            }
        },
    }
    swagger_config = {"headers": [], 'title': 'MATC ASSESSMENT',
                      "openapi": "3.0.3",
                      "specs": [{
                          "endpoint": 'app', "route": '/apidocs',
                      }],
                      "swagger_ui": True, "specs_route": "/app/apidocs/",
                      'swagger_ui_bundle_js': '//unpkg.com/swagger-ui-dist@3'
                                              '/swagger-ui-bundle.js',
                      'swagger_ui_standalone_preset_js': '//unpkg.com'
                                                         '/swagger-ui-dist@3'
                                                         '/swagger-ui'
                                                         '-standalone-preset'
                                                         '.js',
                      'jquery_js': '//unpkg.com/jquery@2.2.4/dist/jquery.min'
                                   '.js',
                      'swagger_ui_css': '//unpkg.com/swagger-ui-dist@3'
                                        '/swagger-ui.css'}

    Swagger(app, config=swagger_config, template=swagger_template)
    db.init_app(app)
    with app.app_context():
        db.create_all()
    return app
