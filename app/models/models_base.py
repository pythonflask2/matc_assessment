from app import *


class BaseModel:

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return self
        except Exception as e:
            print(e)

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return self
        except Exception as e:
            print(e)
