import json
import pymongo
from bson import json_util
connection_url = 'mongodb://localhost:27017/'
databases = pymongo.MongoClient(connection_url)

database_name = 'employee'
database = databases[database_name]

collection_name = 'employee_details'
collection_name2 = 'address'
collection_name3 = 'person'
collection = database[collection_name]
collection2 = database[collection_name2]
collection3 = database[collection_name3]

print(databases.list_database_names())


def insert(employee_name, role, admin, contact_number, date_of_join):
    document = {"employee_Name": employee_name,
                "Role ": role,
                "admin ": admin,
                "contact_number": contact_number,
                "date_of_join": date_of_join
                }
    inserted_id = collection.insert_one(document).inserted_id
    print(inserted_id)
    return f'Document inserted successfully{inserted_id}'


def get_all_details():
    emp_details = collection.find()
    for employee in emp_details:
        print(employee)
        return json.loads(json_util.dumps(employee))

    return 'success'


def get_one_details():
    employee_details = collection.find_one({"employee_Name": "tests"})
    print(employee_details)
    return json.loads(json_util.dumps(employee_details))

