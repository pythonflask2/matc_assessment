import logging

from flask import jsonify

from app import db
from app.models.employee import Employee
from app.models.models_base import *


class Person(db.Model, BaseModel):
    __table_name__ = 'person'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    employee_id = db.Column(db.Integer, db.ForeignKey('employee.id'),
                            nullable=False)
    person_name = db.Column(db.String(100))
    age = db.Column(db.Integer)
    dob = db.Column(db.DATE)

    @classmethod
    def save_person(cls, person_name, age, dob,
                    employee_id):
        person = Person()
        person.person_name = person_name
        person.age = age
        person.dob = dob
        Employee.get_employee_details_by_id(employee_id)
        employee_id = employee_id
        person.employee_id = employee_id
        person.save()
        # db.session.add(person)
        # db.session.commit()
        logging.info('Added person details')
        return 'Person details added'

    @classmethod
    def get_person_details_by_id(cls, id):
        person = Person.query.filter_by(id=id).all()
        return person

    @classmethod
    def get_all_person_details(cls):
        person = Person.query.all()
        return person

    @classmethod
    def delete_person(cls, id):
        person = Person.query.get(id)
        db.session.delete(person)
        db.session.commit()
        logging.info('Person details deleted')
        return jsonify({'message': "Person details deleted"})
