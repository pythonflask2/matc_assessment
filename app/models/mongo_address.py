import json

from bson import json_util

from app.models.mongo_db import collection2, collection, databases


def insert_address(employee_id, place_of_birth, city, nationality):
    document = {"place_of_birth": place_of_birth,
                "city": city,
                "nationality": nationality,
                "employee_id": employee_id
                }
    collection2.insert_one(document)
    return f'The values are inserted into address: {document}'


def get_all_details():
    address_details = collection2.find()
    for address in address_details:
        print(address)
        return json.loads(json_util.dumps([address]))

    return 'success'
