import logging
import uuid

from flask import jsonify
from app.models.models_base import *
from app import db


class Employee(db.Model, BaseModel):
    __table_name__ = 'employee'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    employee_name = db.Column(db.String(100))
    admin = db.Column(db.Boolean)
    external_id = db.Column(db.String(36), default=lambda: str(uuid.uuid4()),
                            nullable=False, index=True, unique=True)
    role = db.Column(db.String(100))
    date_of_join = db.Column(db.DATETIME)
    contact_number = db.Column(db.String(150), unique=True)
    employee = db.relationship('Address', backref='employee', lazy='select')

    @classmethod
    def create_employee(cls,
                        role, contact_number, employee_name, date_of_join,
                        admin):
        employee = Employee()
        employee.admin = admin
        employee.employee_name = employee_name
        employee.role = role
        employee.contact_number = contact_number
        employee.date_of_join = date_of_join
        employee.save()
        logging.info(f'Employee details')
        return 'Employee details inserted'

    @classmethod
    def get_employee_details_by_id(cls, id):
        employee = Employee.query.filter_by(id=id)
        return employee

    @classmethod
    def delete_user(cls, id):
        user = Employee.query.get(id)
        db.session.delete(user)
        db.session.commit()
        logging.info(f'Employee details deleted{user}')
        return jsonify({'message': "Employee details deleted"})

    @classmethod
    def getall_employee(cls):
        employee = Employee.query.all()
        return employee
