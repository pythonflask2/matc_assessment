import uuid

from flask import jsonify

from app import db


class Login(db.Model):
    __tablename__ = 'login'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(100))
    password = db.Column(db.String(100))
    admin = db.Column(db.Boolean)


