from flask import Blueprint, request, render_template
from flask_paginate import Pagination, get_page_parameter

from app.models.employee import Employee

paginate = Blueprint('users', __name__)


@paginate.route('/', methods=['GET'])
def get_pages():
    search = False
    q = request.args.get('q')
    ROWS_PER_PAGE = 5
    if q:
        search = True

    page = request.args.get('page', 1, type=int)

    employee = Employee.query.all()
    pagination = Pagination(page=page, total=len(employee), search=search,
                            record_name='employee_details',
                            per_page=ROWS_PER_PAGE)
    return render_template('index.html',
                           employee_details=employee,
                           pagination=pagination
                           )