import logging
import time
from jaeger_client import Config


def log_tracer():
    logging.getLogger('').handlers = []
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s [%(levelname)s] %(message)s")
    config = Config(
        config={  # usually read from some yaml config
            'sampler': {
                'type': 'const',
                'param': 1,
            },
            'logging': True,
        },
        service_name='matc_assessment',
        validate=True,
    )
    # this call also sets opentracing.tracer
    tracer = config.initialize_tracer()
    with tracer.start_span(operation_name='Matc_Assessement') as span:
        span.set_tag('error', '')
        span.log_event(event=200 or 500)
    with tracer.start_span('matc_assessement',
                           child_of=span) as child_span:
        child_span.log_event(event=200 or 500)

    time.sleep(2)
    tracer.close()


