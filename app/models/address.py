from flask import jsonify
import logging

from app.models.employee import Employee
from app.models.models_base import *


class Address(db.Model, BaseModel):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    employee_id = db.Column(
        db.Integer, db.ForeignKey('employee.id'), nullable=False)
    place_of_birth = db.Column(db.String(100))
    city = db.Column(db.String(100))
    nationality = db.Column(db.String(100))

    @classmethod
    def save_address(cls, city, place_of_birth, nationality, employee_id):
        address = Address()
        address.city = city
        address.nationality = nationality
        address.place_of_birth = place_of_birth
        Employee.get_employee_details_by_id(employee_id)
        employee_id = employee_id
        address.employee_id = employee_id

        address.save()
        logging.info('Address added')
        return "Address details added"

    @classmethod
    def get_details(cls, id):
        address = Address.query.filter_by(id=id).all()
        return address

    @classmethod
    def get_all_address(cls):
        address = Address.query.all()
        return address

    @classmethod
    def delete_details(cls, id):
        address = Address.query.get(id)
        db.session.delete(address)
        db.session.commit()
        logging.info('Address details added')
        return jsonify({'message': "Address details deleted"})
