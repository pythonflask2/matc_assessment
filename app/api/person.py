from flask import Blueprint
from app.services.person import add_person_details, get_all_person_details, \
    get_person_details, delete_person_by_id
from app.services.login import token_required
from flasgger.utils import swag_from
from app.api.person_swagger import person_post_specs, get_all_person_specs, get_person_specs, delete_person_specs
person = Blueprint('person', __name__)


@person.route('/personadd', methods=['POST'])
@swag_from(person_post_specs, methods=['POST'])
@token_required
def add_person(current_emp):
    return add_person_details()


@person.route('/getperson', methods=['GET'])
@swag_from(get_all_person_specs, methods=['GET'])
@token_required
def get_person(current_emp):
    return get_all_person_details()


@person.route('/getperson_id', methods=['GET'])
@swag_from(get_person_specs, methods=['GET'])
@token_required
def get_person_by_id(current_emp):
    return get_person_details()


@person.route('/deleteperson', methods=['DELETE'])
@swag_from(delete_person_specs, methods=['DELETE'])
@token_required
def delete_person_id(current_emp):
    return delete_person_by_id()

