from flask import Blueprint
from app.models.mongo_db import get_all_details, get_one_details
from app.services.login import auth_token
from app.services.mongo_employee import create_mongo
mongo_employee = Blueprint('mongo', __name__)


@mongo_employee.route('/sample', methods=['POST'])
@auth_token
def inset_details(current_emp):
    return create_mongo(current_emp)


@mongo_employee.route('/get_details', methods=['GET'])
def get_employee_details():
    return get_all_details()


@mongo_employee.route('/get_one', methods=['GET'])
def get_emp_details():
    return get_one_details()

