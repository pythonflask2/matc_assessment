person_post_specs = {
    'security': [{
        'jwt': []
    }],
    "tags": [
        {
            "name": "Person",
        }
    ],
    "requestBody": {
        "content": {
            "multipart/form-data": {
                "schema": {
                    "type": "object",
                    "properties": {
                        "data": {
                            "type": "object",
                            "properties": {
                                "data": {
                                    "required": [
                                        "person_name",
                                        "age",
                                        "dob",
                                        "employee_id"
                                    ],
                                    "properties": {
                                        "person_name": {
                                            "type": "string"
                                        },
                                        "age": {
                                            "type": "integer"
                                        },
                                        "dob": {
                                            "type": "string"
                                        },
                                        "employee_id": {
                                            "type": "integer"
                                        },
                                    },
                                    "type": "object"
                                }
                            }
                        },
                    },
                    "required": [
                        "data"
                    ],
                }
            },
        },
    },
    "responses": {
        "200": {
            "description": "Success",
            "content": {
                "application/json": {
                    "example": {
                        "data": {
                            "person_name": 'string',
                            "age": "Integer",
                            "dob": "date",
                            "employee_id": "Integer"
                        }
                    },
                }
            }
        },
        "400": {
            "description": "Validation error",
            "content": {
                "application/json": {
                    "example": {
                        "errors": [
                            {
                                "code": "INSERT.ERROR",
                                "detail": {
                                    "error": "string"
                                },
                                "status": 400,
                                "title": "Error while Inserting the employee "
                                         "details "
                            }
                        ]
                    }
                }
            }
        }
    },

}

get_person_specs = {
    'components': {
        'securitySchemes': {
            'jwt': {
                'type': "http",
                # 'scheme': "bearer",
                'in': "header",
                'bearerFormat': "JWT"
            },
        }
    },
    'security': [{
        'jwt': []
    }],
    "tags": [
        {
            "name": "Person",
        }
    ],
    "parameters": [
        {
            "in": "path",
            'name': 'id',
            "required": True,
            "schema": {
                "type": "string"
            }
        },
    ],
    "responses": {
        "200": {
            "description": "success",
            "content": {
                "application/json": {
                    "example": {
                        "data": {
                            "person_name": "string",
                            "age": "Integer",
                            "dob": "Integer",
                            "employee_id": "Integer",
                            "id": "integer"
                        }
                    },
                }
            }
        },
        "400": {
            "description": "Validation error",
            "content": {
                "application/json": {
                    "example": {
                        "errors": [
                            {
                                "code": "GET_DOCUMENT.ERROR",
                                "detail": {
                                    "error": "string"
                                },
                                "status": 400,
                                "title":
                                    "Error while getting employee details "
                            }
                        ]
                    }
                }
            }
        }
    },
}

get_all_person_specs = {
    'security': [{
        'jwt': []
    }],
    "tags": [
        {
            "name": "Person",
        }
    ],
    "responses": {
        "200": {
            "description": "success",
            "content": {
                "application/json": {
                    "example": {
                        "data": {
                            "person_name": "string",
                            "age": "integer",
                            "dob": "dateTime",
                            "employee_id": "integer",
                            "id": "integer"
                        }

                    },
                }
            }
        },
        "400": {
            "description": "Validation error",
            "content": {
                "application/json": {
                    "example": {
                        "errors": [
                            {
                                "code": "GET VALUE.ERROR",
                                "detail": {
                                    "error": "string"
                                },
                                "status": 400,
                                "title":
                                    "Error while getting employee details "
                            }
                        ]
                    }
                }
            }
        }
    },
}

delete_person_specs = {
    'components': {
        'securitySchemes': {
            'jwt': {
                'type': "http",
                # 'scheme': "bearer",
                'in': "header",
                'bearerFormat': "JWT"
            },
        }
    },
    'security': [{
        'jwt': []
    }],
    "tags": [
        {
            "name": "Person",
        }
    ],
    "parameters": [
        {
            "name": 'id',
            "in": "path",
            "required": True,
            "schema": {
                "type": "string"
            }
        },
    ],
    "responses": {
        "200": {
            "description": "success",
            "content": {
                "application/json": {
                    "example": "DELETED the values"
                }
            }
        },
        "400": {
            "description": "Validation error",
            "content": {
                "application/json": {
                    "example": {
                        "errors": [
                            {
                                "code": "DELETE EMPLOYEE.ERROR",
                                "detail": {
                                    "error": "string"
                                },
                                "status": 400,
                                "title":
                                    "Error while deleting employee details "
                            }
                        ]
                    }
                }
            }
        }
    },
}
