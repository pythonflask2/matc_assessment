from app.api.swagger import post_document_specs, get_document_specs, \
    get_employee_specs, delete_employee_specs
from app.services.employee import create_and_send, get_all_users, \
    delete_employee, get_single_employee
from flask import Blueprint
from flasgger.utils import swag_from
from app.services.login import token_required

employee = Blueprint('employee', __name__)


@employee.route('/insert', methods=['POST'])
@swag_from(post_document_specs, methods=['POST'])
@token_required
def employee_details(current_emp):
    return create_and_send(current_emp)


@employee.route('/getall', methods=['GET'])
@swag_from(get_employee_specs, methods=['GET'])
@token_required
def get_employee_details(current_emp):
    return get_all_users(current_emp)


@employee.route('/get/<int:id>', methods=['GET'])
@swag_from(get_document_specs, methods=['GET'])
@token_required
def get_employee(current_emp, id):
    return get_single_employee(current_emp, id=id)


@employee.route('/delete/<int:id>', methods=['DELETE'])
@swag_from(delete_employee_specs, methods=['DELETE'])
@token_required
def delete(current_emp, id):
    return delete_employee(current_emp, id=id)
