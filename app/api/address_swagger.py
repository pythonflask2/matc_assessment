
class Address:
    address_post_specs = {
        'security': [{
            'jwt': []
        }],
        "tags": [
            {
                "name": "Address",
            }
        ],
        "requestBody": {
            "content": {
                "multipart/form-data": {
                    "schema": {
                        "type": "object",
                        "properties": {
                            "data": {
                                "type": "object",
                                "properties": {
                                    "data": {
                                        "required": [
                                            "city",
                                            "place_of_birth",
                                            "nationality",
                                            "employee_id"
                                        ],
                                        "properties": {
                                            "city": {
                                                "type": "string"
                                            },
                                            "place_of_birth": {
                                                "type": "string"
                                            },
                                            "nationality": {
                                                "type": "string"
                                            },
                                            "employee_id": {
                                                "type": "integer"
                                            },
                                        },
                                        "type": "object"
                                    }
                                }
                            },
                        },
                        "required": [
                            "data"
                        ],
                    }
                },
            },
        },
        "responses": {
            "200": {
                "description": "Success",
                "content": {
                    "application/json": {
                        "example": {
                            "data": {
                                "city": 'string',
                                "nationality": "string",
                                "place_of_birth": "string",
                                "employee_id": "integer"
                            }
                        },
                    }
                }
            },
            "400": {
                "description": "Validation error",
                "content": {
                    "application/json": {
                        "example": {
                            "errors": [
                                {
                                    "code": "INSERT.ERROR",
                                    "detail": {
                                        "error": "string"
                                    },
                                    "status": 400,
                                    "title": "Error while Inserting the employee "
                                             "details "
                                }
                            ]
                        }
                    }
                }
            }
        },

    }

    get_address_specs = {
        'components': {
            'securitySchemes': {
                'jwt': {
                    'type': "http",
                    'scheme': "bearer",
                    'in': "header",
                    'bearerFormat': "JWT"
                },
            }
        },
        'security': [{
            'jwt': []
        }],
        "tags": [
            {
                "name": "Address",
            }
        ],
        "parameters": [
            {
                "in": "path",
                'name': 'id',
                "required": True,
                "schema": {
                    "type": "string"
                }
            },
        ],
        "responses": {
            "200": {
                "description": "success",
                "content": {
                    "application/json": {
                        "example": {
                            "data": {
                                "city": "string",
                                "nationality": "string",
                                "place_of_birth": "string",
                                "employee_id": "string",
                                "id": "integer"
                            }
                        },
                    }
                }
            },
            "400": {
                "description": "Validation error",
                "content": {
                    "application/json": {
                        "example": {
                            "errors": [
                                {
                                    "code": "GET_DOCUMENT.ERROR",
                                    "detail": {
                                        "error": "string"
                                    },
                                    "status": 400,
                                    "title":
                                        "Error while getting employee details "
                                }
                            ]
                        }
                    }
                }
            }
        },
    }

    get_all_specs = {
        'security': [{
            'jwt': []
        }],
        "tags": [
            {
                "name": "Address",
            }
        ],
        "responses": {
            "200": {
                "description": "success",
                "content": {
                    "application/json": {
                        "example": {
                            "data": {
                                "admin": "boolean",
                                "contact_number": "string",
                                "date_of_join": "string",
                                "employee_name": "string",
                                "id": "integer",
                                "role": "string"
                            }

                        },
                    }
                }
            },
            "400": {
                "description": "Validation error",
                "content": {
                    "application/json": {
                        "example": {
                            "errors": [
                                {
                                    "code": "GET VALUE.ERROR",
                                    "detail": {
                                        "error": "string"
                                    },
                                    "status": 400,
                                    "title":
                                        "Error while getting employee details "
                                }
                            ]
                        }
                    }
                }
            }
        },
    }

    delete_address_specs = {
        'components': {
            'securitySchemes': {
                'jwt': {
                    'type': "http",
                    'scheme': "bearer",
                    'in': "header",
                    'bearerFormat': "JWT"
                },
            }
        },
        'security': [{
            'jwt': []
        }],
        "tags": [
            {
                "name": "Address",
            }
        ],
        "parameters": [
            {
                "name": 'id',
                "in": "path",
                "required": True,
                "schema": {
                    "type": "string"
                }
            },
        ],
        "responses": {
            "200": {
                "description": "success",
                "content": {
                    "application/json": {
                        "example": "DELETED the values"
                    }
                }
            },
            "400": {
                "description": "Validation error",
                "content": {
                    "application/json": {
                        "example": {
                            "errors": [
                                {
                                    "code": "DELETE EMPLOYEE.ERROR",
                                    "detail": {
                                        "error": "string"
                                    },
                                    "status": 400,
                                    "title":
                                        "Error while deleting employee details "
                                }
                            ]
                        }
                    }
                }
            }
        },
    }
