from bson import json_util

from app.models.mongo_db import collection3
from app.services.login import auth_token
from app.services.mongo_person import add_person_details, \
    get_all_person_details, get_one_person_details
from flask import Blueprint, jsonify

mongo_person = Blueprint('mongo_person', __name__)


@mongo_person.route('/increase', methods=['POST'])
@auth_token
def insert_person(current_emp):
    return add_person_details()


@mongo_person.route('/get_person', methods=['GET'])
def get_all_person():
    return get_all_person_details()


@mongo_person.route('/person', methods=['Get'])
def get_single_person():
    return get_one_person_details()
