from app.services.login import login_user, signup_user
from flask import Blueprint

login = Blueprint('login', __name__)


@login.route('/login', methods=['GET'])
def login_employee():
    return login_user()


@login.route('/register', methods=['GET', 'POST'])
def register_emp():
    return signup_user()
