from flask import Blueprint

from app.services.login import auth_token
from app.services.mongo_address import insert_address_details

mongo_address = Blueprint('mongo_address', __name__)


@mongo_address.route('/address', methods=['POST'])
@auth_token
def add_address(current_emp):
    return insert_address_details()

