from flask import Blueprint
from flasgger.utils import swag_from
from app.services.address import create_address, get_employe_address, \
    get_details_by_id, delete_details_by_id
from app.api.address_swagger import Address
from app.services.login import token_required

address = Blueprint('address', __name__)


@address.route('/add', methods=['POST'])
@swag_from(Address.address_post_specs, methods=['POST'])
@token_required
def add_address(current_emp):
    return create_address()


@address.route('/getdetails', methods=['GET'])
@swag_from(Address.get_all_specs, methods=['GET'])
@token_required
def get_emp_address(current_emp):
    return get_employe_address()


@address.route('/getbyone/<int:id>', methods=['GET'])
@swag_from(Address.get_address_specs, methods=['GET'])
@token_required
def get_one_by_id(current_emp, id):
    return get_details_by_id(id)


@address.route('/remove/<int:id>', methods=['DELETE'])
@swag_from(Address.delete_address_specs, methods=['DELETE'])
@token_required
def delete(current_emp, id):
    return delete_details_by_id(id)
