post_document_specs = {
    'security': [{
        'jwt': []
    }],
    "tags": [
        {
            "name": "Employee"
        }
    ],
    "requestBody": {
        "content": {
            "multipart/form-data": {
                "schema": {
                    "type": "object",
                    "properties": {
                        "data": {
                            "type": "object",
                            "properties": {
                                "data": {
                                    "required": [
                                        "employee_name",
                                        "admin",
                                        "role",
                                        "date_of_join",
                                        "contact_number"
                                    ],
                                    "properties": {
                                        "employee_name": {
                                            "type": "string"
                                        },
                                        "admin": {
                                            "type": "boolean"
                                        },
                                        "role": {
                                            "type": "string"
                                        },
                                        "contact_number": {
                                            "type": "integer"
                                        },
                                        "date_of_join": {
                                            "type": "string"
                                        },
                                    },
                                    "type": "object"
                                }
                            }
                        },
                    },
                    "required": [
                        "data"
                    ],
                }
            },
        },
    },
    "responses": {
        "200": {
            "description": "Success",
            "content": {
                "application/json": {
                    "example": {
                        "data": {
                            "admin": 'boolean',
                            "contact_number": "integer",
                            "date_of_join": "string",
                            "employee_name": "string",
                            "role": "string"
                        }
                    },
                }
            }
        },
        "400": {
            "description": "Validation error",
            "content": {
                "application/json": {
                    "example": {
                        "errors": [
                            {
                                "code": "INSERT.ERROR",
                                "detail": {
                                    "error": "string"
                                },
                                "status": 400,
                                "title": "Error while Inserting the employee "
                                         "details "
                            }
                        ]
                    }
                }
            }
        }
    },

}

get_document_specs = {
    'components': {
        'securitySchemes': {
            'jwt': {
                'type': "http",
                'scheme': "bearer",
                'in': "header",
                'bearerFormat': "JWT"
            },
        }
    },
    'security': [{
        'jwt': []
    }],
    "tags": [
        {
            "name": "Employee",
        }
    ],
    "parameters": [
        {
            "in": "path",
            'name': 'employee_name',
            "required": True,
            "schema": {
                "type": "string"
            }
        },
    ],
    "responses": {
        "200": {
            "description": "success",
            "content": {
                "application/json": {
                    "example": {
                        "data": {
                            "admin": "boolean",
                            "contact_number": "integer",
                            "date_of_join": "string",
                            "employee_name": "string",
                            "id": "integer",
                            "role": "string"
                        }
                    },
                }
            }
        },
        "400": {
            "description": "Validation error",
            "content": {
                "application/json": {
                    "example": {
                        "errors": [
                            {
                                "code": "GET_DOCUMENT.ERROR",
                                "detail": {
                                    "error": "string"
                                },
                                "status": 400,
                                "title":
                                    "Error while getting employee details "
                            }
                        ]
                    }
                }
            }
        }
    },
}

get_employee_specs = {
    'security': [{
        'jwt': []
    }],
    "tags": [
        {
            "name": "Employee",
        }
    ],
    "responses": {
        "200": {
            "description": "success",
            "content": {
                "application/json": {
                    "example": {
                        "data": {
                            "admin": "boolean",
                            "contact_number": "integer",
                            "date_of_join": "string",
                            "employee_name": "string",
                            "id": "integer",
                            "role": "string"
                        }

                    },
                }
            }
        },
        "400": {
            "description": "Validation error",
            "content": {
                "application/json": {
                    "example": {
                        "errors": [
                            {
                                "code": "GET VALUE.ERROR",
                                "detail": {
                                    "error": "string"
                                },
                                "status": 400,
                                "title":
                                    "Error while getting employee details "
                            }
                        ]
                    }
                }
            }
        }
    },
}

delete_employee_specs = {
    'components': {
        'securitySchemes': {
            'jwt': {
                'type': "http",
                'scheme': "bearer",
                'in': "header",
                'bearerFormat': "JWT"
            },
        }
    },
    'security': [{
        'jwt': []
    }],
    "tags": [
        {
            "name": "Employee",
        }
    ],
    "parameters": [
        {
            "name": 'id',
            "in": "path",
            "required": True,
            "schema": {
                "type": "string"
            }
        },
    ],
    "responses": {
        "200": {
            "description": "success",
            "content": {
                "application/json": {
                    "example": "DELETED the values"
                }
            }
        },
        "400": {
            "description": "Validation error",
            "content": {
                "application/json": {
                    "example": {
                        "errors": [
                            {
                                "code": "DELETE EMPLOYEE.ERROR",
                                "detail": {
                                    "error": "string"
                                },
                                "status": 400,
                                "title":
                                    "Error while deleting employee details "
                            }
                        ]
                    }
                }
            }
        }
    },
}
