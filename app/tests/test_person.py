import unittest
from app.models.person import Person
from app.tests.test_base import TestBase


class TestPerson(TestBase):
    def test_create_person(self):
        person = Person.save_person('tests', 21,
                                    '2000-06-15T13:45:30', 2)
        print(person)
        self.assertEqual(0, 0)

    def test_get_by_id(self):
        id = 1
        person = Person.get_person_details_by_id(id=id)
        print(person)
        self.assertEqual(0, 0)

    def test_get_all_person(self):
        person = Person.get_all_person_details()
        print(person)

    def test_delete(self):
        id = 3
        person = Person.delete_person(id=id)
        print('person deleted')


if __name__ == '__main__':
    unittest.main()
