import unittest

from app.models.address import Address
from app import create_app
from app.tests.test_base import TestBase


class TestAddress(TestBase):

    def test_create_address(self):
        address = Address.save_address('test', 'test', 'test', '2')
        print(address)
        self.assertEqual(0, 0)

    def test_get_address_details(self):
        id = 1
        address = Address.get_details(id=id)
        print(address)
        self.assertEqual(0, 0)

    def test_get_all_addrress_details(self):
        address_details_all = Address.get_all_address()
        print(address_details_all)

    def test_delete_address(self):
        id = 4
        delete_address = Address.delete_details(id=id)
        print('address deleted')


if __name__ == '__main__':
    unittest.main()

# class TestAddressBase(BaseTest):
#     def test_create_address(self):
#         address = Address.save_address('abcde', 'efgh', 'Indian', '1')
#         print(address)
#
#
# if __name__ == '__main__':
#     unittest.main()
