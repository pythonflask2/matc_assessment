import unittest

from app import db
from app.models.employee import Employee
from app.tests.test_base import TestBase, BaseTest


class TestEmployeeModel(BaseTest):
    def test_create(self):
        employee = Employee.create_employee('test', 'test',
                                            '2022-06-21T12:51:55.773Z',
                                            'true', '9023456713')
        print(employee)

        self.assertEqual(0, 0)

    def _test_list_by_query(self, employee_object):
        _filter_dict = {'source': 'test'}
        self.employee.get_employee_details_by_id(_filter_dict)
        self.assertEqual(self.employee.id, employee_object.id)

    def test_by_id(self):
        resouce_object = None
        for orm_obj in self.all_orm_objs:
            resouce_object = orm_obj.get_employee_details_by_id(orm_obj.id)
        self._test_list_by_query(resouce_object)

    # def _test_get(self, employee):
    #     id = employee.id
    #     employee = Employee.get_employee_details_by_id(id=id)
    #     self.assertEqual(0, 0)


class TestEmployee(TestBase):
    def test_create_employee(self):
        employee = Employee.create_employee('tests', '9065785432', 'tests',
                                            '2021-08-08 00:00:00', True)
        self.assertEqual(0, 0)

    def test_get(self):
        employee = Employee.get_employee_details_by_id(id=1)
        print(employee)
        self.assertEqual(0, 0)

    def test_get_all(self):
        employee_all = Employee.getall_employee()
        print(employee_all)
        self.assertEqual(0, 0)

    def test_delete(self):
        id = 4
        delete_employee = Employee.delete_user(id=id)
        print('Employee details deleted')
        self.assertEqual(0, 0)


if __name__ == '__main__':
    unittest.main()
