import os
import unittest

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker

from app import db, app
from app.models import models_base
from app.models.address import Address
from app.models.employee import Employee
from app.models.models_base import BaseModel
from app.models.person import Person


class BaseTest(unittest.TestCase):
    employee = None
    address = None
    person = None
    engine = None

    @classmethod
    def setUpClass(cls):
        e = create_engine(
            "sqlite:///:memory:?cache=shared"
            "&check_same_thread=false&uri=true"
        )
        cls.engine = e
        db.metadata.create_all(cls.engine)
        Employee.db = scoped_session(sessionmaker(bind=cls.engine,
                                                  expire_on_commit=False))
        employee = Employee()
        employee.id = 1
        employee.employee_name = 'test'
        employee.admin = True
        employee.role = 'test'
        employee.date_of_join = "2021-08-08"
        employee.contact_number = '9087654321'
        employee.save()
        cls.employee = employee
        cls.all_orm_objs = [
            cls.employee
        ]

    @classmethod
    def tearDownClass(cls):
        Employee.db.close()
        cls.engine.dispose()
        root = os.getcwd()
        for file in os.listdir(root):
            if file.startswith(":memory"):
                os.remove(os.path.join(root, file))


import unittest
from app import create_app

flapp = create_app()


class TestBase(unittest.TestCase):
    def setUp(self):
        self.app = flapp
        self.appctx = self.app.app_context()
        self.appctx.push()


