import json

from bson import json_util
from flask import request, jsonify

from app.models.mongo_db import collection3
from app.models.mongo_person import insert_person_details


def add_person_details():
    person_name = request.json.get('person_name')
    age = request.json.get('age')
    dob = request.json.get('dob')
    employee_id = request.json.get('employee_id')
    return insert_person_details(person_name, age, dob, employee_id)


def get_all_person_details():
    person_details = collection3.find()
    data = []
    for person in person_details:
        data.append(person)
        print(data)
        # return json.loads(json_util.dumps(data))
    return 'success'


def get_one_person_details():
    person_details = collection3.find_one({"person_name": "21"})
    print(person_details)
    return json.loads(json_util.dumps(person_details))
