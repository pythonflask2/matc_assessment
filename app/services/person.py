import json
import logging

from flask import request, jsonify

from app.models.person import Person


def add_person_details():
    request_form_data = json.loads(request.form.get('data'))
    req_data: dict = request_form_data['data']
    person_name = req_data['person_name']
    age = req_data['age']
    dob = req_data['dob']
    employee_id = req_data['employee_id']
    return Person.save_person(person_name, age, dob, employee_id)


def get_all_person_details():
    person = Person.get_all_person_details()

    result = []

    for person_details in person:
        person_data = {}
        person_data['person_name'] = person_details.person_name
        person_data['age'] = person_details.age
        person_data['dob'] = person_details.dob
        person_data['employee_id'] = person_details.employee_id

        result.append(person_data)
    return jsonify({'employee': result})


def get_person_details():
    id = request.form.get('id')
    user = Person.get_person_details_by_id(id)
    person_details = []
    for value in user:
        value = value.__dict__
        value.pop("_sa_instance_state")
        person_details.append(value)
        logging.info(f'The person details for the {id} is{person_details}')
    return jsonify(person_details)


def delete_person_by_id():
    id = int(request.form.get('id'))
    return Person.delete_person(id=id)
