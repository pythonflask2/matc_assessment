import json
import logging
from flask import request, jsonify

from app.models.address import Address


def create_address():
    request_form_data = json.loads(request.form.get('data'))
    req_data: dict = request_form_data['data']
    city  = req_data['city']
    nationality = req_data['nationality']
    place_of_birth = req_data['place_of_birth']
    employee_id = req_data['employee_id']
    return Address.save_address(
        city, nationality, place_of_birth, employee_id)


def get_employe_address():
    address = Address.get_all_address()
    output = []
    for address_details in address:
        employee_data = {}
        employee_data['city'] = address_details.city
        employee_data['nationality'] = address_details.nationality
        employee_data['employee_id'] = address_details.employee_id
        employee_data['place_of_bith'] = address_details.place_of_birth
        output.append(employee_data)
    logging.info('Employee lists')
    return jsonify({'list_of_employees': output})


def get_details_by_id(id):
    user = Address.get_details(id)
    address_details = []
    for value in user:
        value = value.__dict__
        value.pop("_sa_instance_state")
        address_details.append(value)
        logging.info(f'The Addresses details for the {id} is{address_details}')
    return jsonify(address_details)


def delete_details_by_id(id):
    return Address.delete_details(id=id)
