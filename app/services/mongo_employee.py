from flask import request

from app.models.mongo_db import insert


def create_mongo(current_emp):
    if current_emp.admin:
        employee_name = request.form.get('employee_name')
        role = request.form.get('role')
        admin = request.form.get('admin')
        contact_number = request.form.get('contact_number')
        date_of_join = request.form.get('date_of_join')
        return insert(employee_name, role, admin, contact_number, date_of_join)
    else:
        return 'You Dont have access to this page'

