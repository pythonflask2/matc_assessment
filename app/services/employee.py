import json
import logging
from app.models.base import log_tracer
from flask import request, jsonify

from app.models.employee import Employee


class InvalidError(Exception):
    def __init__(self, message):
        self.message = message


def create_and_send(current_emp):
    try:
        if current_emp.admin:
            request_form_data = json.loads(request.form.get('data'))
            req_data: dict = request_form_data['data']
            role = req_data['role']
            contact_number = req_data['contact_number']
            employee_name = req_data['employee_name']
            date_of_join = req_data['date_of_join']
            admin = req_data['admin']
            emp = Employee.create_employee(
                 role, contact_number, employee_name, date_of_join, admin)
            return emp
        else:
            return jsonify({
                'message': "OOPS!, You don't have access to this page"})
    except:
        raise InvalidError('Incorrect datetime value')


def get_all_users(current_emp):
    try:
        if current_emp.admin:
            employee = Employee.getall_employee()

            result = []

            for employee_details in employee:
                employee_data = {}
                employee_data['admin'] = employee_details.admin
                employee_data[
                    'employee_name'] = employee_details.employee_name
                employee_data['role'] = employee_details.role
                employee_data[
                    'date_of_join'] = employee_details.date_of_join
                employee_data[
                    'contact_number'] = employee_details.contact_number

                result.append(employee_data)
            logging.info(f'Employee Details{result}')
            return jsonify({'employee': result})
        else:
            return jsonify({'message': 'dont have an access'})

    except:
        raise InvalidError('There is no result in the employee table')


def get_single_employee(current_emp, id):
    try:
        if current_emp.admin:
            user = Employee.get_employee_details_by_id(id)
            user_details = []
            for value in user:
                value = value.__dict__
                value.pop("_sa_instance_state")
                user_details.append(value)
            logging.info(f'The user_details for the {id} is{user_details}')

            return jsonify(user_details)

        else:
            logging.error("You don't have access")
            return jsonify({"message": "Admin only have the acceess"})
    except InvalidError as e:
        print('the given id is invalid')


def delete_employee(current_emp, id):
    try:
        if current_emp.admin:
            return Employee.delete_user(id=id)

        else:
            log_tracer().error("You don't have access")
            return jsonify({
                'message': 'Only admin has the access to delete the '
                           'details'})
    except InvalidError as e:
        print('The given id is invalid', e)
