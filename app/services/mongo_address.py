from flask import request
from app.models.mongo_address import insert_address


def insert_address_details():
    place_of_birth = request.form.get('place_of_birth')
    city = request.form.get('city')
    nationality = request.form.get('nationality')
    return insert_address(place_of_birth, city, nationality)



