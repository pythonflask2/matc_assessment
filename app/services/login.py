import datetime
import logging
from functools import wraps
import jwt
from flask import request, make_response, jsonify
from werkzeug.security import check_password_hash, generate_password_hash
from app import app, db
from app.models.login import Login


def signup_user():
    data = request.get_json()

    hashed_password = generate_password_hash(data['password'], method='sha256')

    new_emp = Login(id=Login.id, username=data['username'],
                    password=hashed_password, admin=True)
    db.session.add(new_emp)
    db.session.commit()
    logging.info('Registered successfully')
    return jsonify({'message': 'registered successfully'})


def token_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        token = None
        if 'Authorization' in request.headers:
            bearer = request.headers['Authorization']
            token = bearer.split()[1]  # YourTokenHere

        if not token:
            return jsonify({'message': 'a valid token is missing'})

        try:
            data = jwt.decode(token, app.config['SECRET_KEY'])
            current_emp = Login.query.filter_by(id=data['id']).first()
        except:
            return jsonify({'message': 'token is invalid'})
        return f(current_emp, *args, **kwargs)

    return decorator


def auth_token(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        access_token = None
        if 'access_tokens' in request.headers:
            access_token = request.headers['access_tokens']

        if not access_token:
            return jsonify({'message': 'a valid token is missing'})

        try:
            data = jwt.decode(access_token, app.config['SECRET_KEY'])
            current_emp = Login.query.filter_by(id=data['id']).first()
        except:
            return jsonify({'message': 'token is invalid'})

        return f(current_emp, *args, **kwargs)

    return decorator


def login_user():
    auth = request.authorization

    if not auth or not auth.username or not auth.password:
        return make_response('could not verify', 401, {
            'WWW.Authentication': 'Basic realm: "login required"'})

    employee = Login.query.filter_by(username=auth.username).first()

    if check_password_hash(employee.password, auth.password):
        token = jwt.encode({'id': employee.id,
                            'exp': datetime.datetime.utcnow() + datetime.timedelta(
                                minutes=50)}, app.config['SECRET_KEY'])
        return jsonify({'token': token.decode('UTF-8')})
    logging.error('could not verify')
    return make_response('could not verify', 401, {
        'WWW.Authentication': 'Basic realm: "login required"'})
