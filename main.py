from app import create_app
from app.models.base import log_tracer

app = create_app()


if __name__ == '__main__':
    log_tracer()
app.run()
