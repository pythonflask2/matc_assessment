from os import environ, path
from dotenv import load_dotenv

basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, '.env'))


def get_env(config_var):
    assert config_var is not None
    config_var = environ.get(config_var)
    return config_var
